# shannon/nvim
Just a bunch of dotfiles.

## Install
Clone and symlink or install with [ellipsis][ellipsis]:

```
$ ellipsis install shannon/nvim
```

[ellipsis]: http://ellipsis.sh
